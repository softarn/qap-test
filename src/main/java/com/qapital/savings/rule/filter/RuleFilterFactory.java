package com.qapital.savings.rule.filter;

import com.qapital.bankdata.transaction.Transaction;
import com.qapital.savings.rule.SavingsRule;
import java.util.function.Predicate;

public class RuleFilterFactory {

    private static final Predicate<Transaction> transactionIsConsumption = transaction -> transaction.getAmount() < 0;

    /**
     * Returns a filter that filters out transactions that aren't applicable to rule
     */
    public static Predicate<Transaction> getFor(SavingsRule savingsRule) {
        String placeDescription = savingsRule.getPlaceDescription();

        switch (savingsRule.getRuleType()) {
            case guiltypleasure:
                return transactionDescriptionMatches(placeDescription).and(transactionIsConsumption);
            case roundup:
                return transactionIsConsumption;
            default:
                throw new IllegalStateException("No rule filter exists for rule type:" + savingsRule.getRuleType());
        }
    }

    private static Predicate<Transaction> transactionDescriptionMatches(String rulePlaceDescription) {
        return (transaction -> transaction.getDescription().equalsIgnoreCase(rulePlaceDescription));
    }

}
