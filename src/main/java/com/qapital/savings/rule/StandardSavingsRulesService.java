package com.qapital.savings.rule;

import static java.util.stream.Collectors.toList;

import com.qapital.bankdata.transaction.Transaction;
import com.qapital.bankdata.transaction.TransactionsService;
import com.qapital.savings.event.SavingsEvent;
import com.qapital.savings.rule.filter.RuleFilterFactory;
import com.qapital.savings.rule.mapper.RuleMapperFactory;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StandardSavingsRulesService implements SavingsRulesService {

    private final TransactionsService transactionsService;

    @Autowired
    public StandardSavingsRulesService(TransactionsService transactionsService) {
        this.transactionsService = transactionsService;
    }

    @Override
    public List<SavingsRule> activeRulesForUser(Long userId) {
        SavingsRule guiltyPleasureRule = SavingsRule.createGuiltyPleasureRule(1l, userId, "Starbucks", 3.00d);
        guiltyPleasureRule.addSavingsGoal(1l);
        guiltyPleasureRule.addSavingsGoal(2l);
        SavingsRule roundupRule = SavingsRule.createRoundupRule(2l, userId, 2.00d);
        roundupRule.addSavingsGoal(1l);

        List<SavingsRule> activeRules = new ArrayList<>();
        activeRules.add(guiltyPleasureRule);
        activeRules.add(roundupRule);

        return activeRules;
    }

    @Override
    public List<SavingsEvent> executeRule(SavingsRule savingsRule) {
        Predicate<Transaction> ruleFilter = RuleFilterFactory.getFor(savingsRule);
        Function<Transaction, List<SavingsEvent>> ruleMapper = RuleMapperFactory.getFor(savingsRule);

        List<Transaction> transactions = transactionsService.latestTransactionsForUser(savingsRule.getUserId());
        return transactions.stream()
            .filter(ruleFilter)     // Filter out transactions that doesn't apply to rule
            .map(ruleMapper)        // Map transactions to list of saving events
            .flatMap(List::stream)  // Flatten lists
            .collect(toList());
    }

}
