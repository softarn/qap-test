package com.qapital.savings.rule.mapper;

import com.qapital.bankdata.transaction.Transaction;
import com.qapital.savings.event.SavingsEvent;
import com.qapital.savings.rule.SavingsRule;
import java.util.List;
import java.util.function.Function;

public class RuleMapperFactory {

    public static Function<Transaction, List<SavingsEvent>> getFor(SavingsRule savingsRule) {
        switch(savingsRule.getRuleType()) {
            case guiltypleasure:
                return new GuiltyPleasureMapper(savingsRule);
            case roundup:
                return new RoundUpMapper(savingsRule);
            default:
                throw new IllegalStateException("No rule mapper exists for rule type:" + savingsRule.getRuleType());
        }
    }
}
