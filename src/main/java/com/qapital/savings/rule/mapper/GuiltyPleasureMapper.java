package com.qapital.savings.rule.mapper;

import static java.util.stream.Collectors.toList;

import com.qapital.bankdata.transaction.Transaction;
import com.qapital.savings.event.SavingsEvent;
import com.qapital.savings.rule.SavingsRule;
import java.util.List;
import java.util.function.Function;
import org.joda.time.LocalDate;

public class GuiltyPleasureMapper implements Function<Transaction, List<SavingsEvent>> {

    private SavingsRule savingsRule;

    public GuiltyPleasureMapper(SavingsRule savingsRule) {
        this.savingsRule = savingsRule;
    }

    public List<SavingsEvent> apply(Transaction transaction) {
        LocalDate now = LocalDate.now();
        int numberOfGoals = savingsRule.getSavingsGoalIds().size();
        double amountPerGoal = savingsRule.getAmount() / numberOfGoals;

        return savingsRule.getSavingsGoalIds().stream()
            .map(savingsGoalId -> createSavingsEvent(amountPerGoal, savingsGoalId, now))
            .collect(toList());
    }

    private SavingsEvent createSavingsEvent(double savingsAmount, Long savingsGoalId, LocalDate date) {
        return new SavingsEvent(
            savingsRule.getUserId(), savingsGoalId, savingsRule.getId(),
            SavingsEvent.EventName.rule_application, date, savingsAmount, 1l, savingsRule
        );
    }
}
