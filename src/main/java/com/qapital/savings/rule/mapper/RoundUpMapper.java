package com.qapital.savings.rule.mapper;

import static java.util.stream.Collectors.toList;

import com.qapital.bankdata.transaction.Transaction;
import com.qapital.savings.event.SavingsEvent;
import com.qapital.savings.rule.SavingsRule;
import java.math.BigDecimal;
import java.util.List;
import java.util.function.Function;
import org.joda.time.LocalDate;

public class RoundUpMapper implements Function<Transaction, List<SavingsEvent>> {

    private final SavingsRule savingsRule;

    public RoundUpMapper(SavingsRule savingsRule) {
        this.savingsRule = savingsRule;
    }

    @Override
    public List<SavingsEvent> apply(Transaction transaction) {
        LocalDate now = LocalDate.now();
        int numberOfGoals = savingsRule.getSavingsGoalIds().size();
        double amountPerGoal = calculateTotalAmount(savingsRule.getAmount(), transaction.getAmount()) / numberOfGoals;

        return savingsRule.getSavingsGoalIds().stream()
            .map(savingsGoalId -> createSavingsEvent(amountPerGoal, savingsGoalId, now))
            .collect(toList());
    }

    private double calculateTotalAmount(double ruleAmount, double transactionAmount) {
        double divisibleBy = ruleAmount % transactionAmount;
        double roundUpToAmount = (divisibleBy + 1) * savingsRule.getAmount();
        return roundUpToAmount - Math.abs(transactionAmount);
    }

    private SavingsEvent createSavingsEvent(double savingsAmount, Long savingsGoalId, LocalDate date) {
        return new SavingsEvent(
            savingsRule.getUserId(), savingsGoalId, savingsRule.getId(),
            SavingsEvent.EventName.rule_application, date, savingsAmount, 1l, savingsRule
        );
    }
}
