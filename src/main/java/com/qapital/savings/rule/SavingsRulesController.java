package com.qapital.savings.rule;

import com.qapital.savings.event.SavingsEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/savings/rule")
public class SavingsRulesController {

    private SavingsRulesService savingsRulesService;

    @Autowired
    public SavingsRulesController(SavingsRulesService savingsRulesService) {
        this.savingsRulesService = savingsRulesService;
    }

    @RequestMapping(value = "/active/{userId}", method = RequestMethod.GET)
    public List<SavingsRule> activeRulesForUser(@PathVariable Long userId) {
        return savingsRulesService.activeRulesForUser(userId);
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public List<SavingsEvent> savingEventsFor(@RequestBody SavingsRule savingsRule) {
        return savingsRulesService.executeRule(savingsRule);
    }

}
