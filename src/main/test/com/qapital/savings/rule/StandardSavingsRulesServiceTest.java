package com.qapital.savings.rule;

import static org.assertj.core.api.Assertions.assertThat;

import com.qapital.bankdata.transaction.Transaction;
import com.qapital.bankdata.transaction.TransactionsService;
import com.qapital.savings.event.SavingsEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.joda.time.LocalDate;
import org.junit.Test;

public class StandardSavingsRulesServiceTest {

    @Test
    public void executeRule_shouldReturnEmptyList_whenNoApplicableExistForUser() {
        long userId = 1l;
        TransactionsService transactionService = userId1 -> new ArrayList<>();

        StandardSavingsRulesService service = new StandardSavingsRulesService(transactionService);

        SavingsRule guiltyPleasureRule = SavingsRule.createGuiltyPleasureRule(1l, userId, "Starbucks", 3.00d);
        guiltyPleasureRule.addSavingsGoal(1l);
        guiltyPleasureRule.addSavingsGoal(2l);

        List<SavingsEvent> savingsEvents = service.executeRule(guiltyPleasureRule);

        assertThat(savingsEvents).isEmpty();
    }

    @Test
    public void executeRule_shouldReturnSavingsEvent_whenUserHasTransactionForGuiltyPleasureRule() {
        long userId = 1l;

        SavingsRule guiltyPleasureRule = SavingsRule.createGuiltyPleasureRule(1l, userId, "Starbucks", 3.00d);
        guiltyPleasureRule.addSavingsGoal(1l);
        guiltyPleasureRule.addSavingsGoal(2l);

        TransactionsService transactionService = userId1 -> Arrays.asList(new Transaction(1l,userId, -5.34d, "Starbucks", new LocalDate(2015,7,1)));

        StandardSavingsRulesService service = new StandardSavingsRulesService(transactionService);

        List<SavingsEvent> savingsEvents = service.executeRule(guiltyPleasureRule);

        assertThat(savingsEvents).isNotEmpty();
        assertThat(savingsEvents).hasSize(2);

        assertThat(savingsEvents.get(0).getAmount()).isEqualTo(1.5d);
    }

    @Test
    public void executeRule_shouldReturnSavingsEvent_whenUserHasTransactionForRoundUpRule() {
        long userId = 1l;

        SavingsRule guiltyPleasureRule = SavingsRule.createRoundupRule(1l, userId, 2.00d);
        guiltyPleasureRule.addSavingsGoal(1l);
        guiltyPleasureRule.addSavingsGoal(2l);

        TransactionsService transactionService = userId1 -> Arrays.asList(new Transaction(1l,userId, -5.3d, "Starbucks", new LocalDate(2015,7,1)));

        StandardSavingsRulesService service = new StandardSavingsRulesService(transactionService);

        List<SavingsEvent> savingsEvents = service.executeRule(guiltyPleasureRule);

        assertThat(savingsEvents).isNotEmpty();
        assertThat(savingsEvents).hasSize(2);
        assertThat(savingsEvents.get(0).getAmount()).isEqualTo(0.35d);
    }


}
