package com.qapital.savings.rule;

import static org.assertj.core.api.Assertions.assertThat;

import com.qapital.bankdata.transaction.Transaction;
import com.qapital.bankdata.transaction.TransactionsService;
import com.qapital.savings.event.SavingsEvent;
import java.util.Arrays;
import java.util.List;
import org.joda.time.LocalDate;
import org.junit.Test;

public class SavingsRulesControllerTest {

    private long userId = 1l;

    @Test
    public void savingsEventsFor_shouldReturnList_whenApplicableTransactionsExists() {
        TransactionsService transactionService = userId1 -> Arrays.asList(
            new Transaction(
                1l,
                userId,
                -5.3d,
                "Starbucks",
                new LocalDate(2015, 7, 1)
            )
        );
        SavingsRulesService service = new StandardSavingsRulesService(transactionService);
        SavingsRulesController controller = new SavingsRulesController(service);

        SavingsRule guiltyPleasureRule = SavingsRule.createRoundupRule(1l, userId, 2.00d);
        guiltyPleasureRule.addSavingsGoal(1l);
        guiltyPleasureRule.addSavingsGoal(2l);

        List<SavingsEvent> body = controller.savingEventsFor(guiltyPleasureRule);

        assertThat(body).isNotEmpty();
        assertThat(body).hasSize(2);
        assertThat(body.get(0).getAmount()).isEqualTo(0.35d);
    }

}