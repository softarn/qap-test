package com.qapital.objectmapper;

import static org.assertj.core.api.Assertions.assertThat;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.test.json.JacksonTester;

public class ObjectMapperTest {

    private JacksonTester<SerializationTest> json;

    @Before
    public void setup() {
        ObjectMapper objectMapper = new CustomObjectMapper();
        JacksonTester.initFields(this, objectMapper);
    }

    @Test
    public void shouldSerializeDate() throws IOException {
        SerializationTest serializationTest = new SerializationTest(LocalDate.parse("2017-01-01"), null);
        assertThat(json.write(serializationTest)).extractingJsonPathStringValue("$.date").isEqualTo("2017-01-01");
    }

    @Test
    public void shouldSerializeDateTime() throws IOException {
        SerializationTest serializationTest = new SerializationTest(null, DateTime.parse("2017-03-21T18:51:18.125Z"));
        assertThat(json.write(serializationTest)).extractingJsonPathStringValue("$.dateTime").isEqualTo("2017-03-21T18:51:18.125Z");
    }

    private class SerializationTest {

        private LocalDate date;
        private DateTime dateTime;

        public SerializationTest(LocalDate date, DateTime dateTime) {
            this.date = date;
            this.dateTime = dateTime;
        }

        public LocalDate getDate() {
            return date;
        }

        public DateTime getDateTime() {
            return dateTime;
        }
    }

}